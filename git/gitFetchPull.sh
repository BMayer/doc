#!/usr/bin/bash

## Timestamp
# Convertir les secondes depuis Epoch (1970-01-01 UTC) en une date : 
# $ date --date='@2147483647'
# $ date --> jeu. 09 févr. 2023 15:11:46 CET
# $ date date --iso-8601=seconds --> 2023-02-09T15:22:01+01:00
# $ date +"%Y%m%dt%H%M%S" --> 20230209t152443
TS="$(date +"%Y%m%dt%H%M%S")"

## Le repertoire de depart
hereDir="$("pwd")"

##  Un repertoire de base du depot
gitDir="/home/bernard/repoGit/"

##  Rapport
logFullName="${gitDir}/gitFetchPull-${TS}.log"
echo "Voir le rapport [${logFullName}]"
echo "[DEBUT] $(date)"                 > "${logFullName}"

##  La liste des chemins des depots
#gitPath[]="${gitDir}/"
#gitPath[]="${gitDir}/"
#gitPath[]="${gitDir}/"

gitPath[0]="${gitDir}/Bateau"
gitPath[1]="${gitDir}/doc"
gitPath[2]="${gitDir}/Electro"
gitPath[3]="${gitDir}/GpxDB"
gitPath[4]="${gitDir}/Java"
gitPath[5]="${gitDir}/LL"
# gitPath[6]="${gitDir}/LME"
gitPath[7]="${gitDir}/meteoCompare"
gitPath[8]="${gitDir}/nmeaDB"
gitPath[9]="${gitDir}/Python"
gitPath[10]="${gitDir}/Resilience"
gitPath[11]="${gitDir}/WpsDB"

for gitRepoPath in "${gitPath[@]}"; do
    echo -e "\n"                       >> "${logFullName}"
    echo "[repo : ${gitRepoPath}]"     >> "${logFullName}"
    cd "${gitRepoPath}"
    #echo -e "\n"                       >> "${logFullName}"
    echo "[status]"                    >> "${logFullName}"
    git status                         >> "${logFullName}" 2>&1
    retCode=$?
    if [[ ${retCode} -eq 0 ]]; then
        #echo -e "\n"                       >> "${logFullName}"
        echo "[fetch]"                 >> "${logFullName}"
        git fetch                      >> "${logFullName}" 2>&1
        #echo -e "\n"                   >> "${logFullName}"
        echo "[pull]"                  >> "${logFullName}"
        git pull                       >> "${logFullName}" 2>&1
    fi
done

cd ${hereDir}
echo -e "\n[FIN]"                      >> "${logFullName}"
